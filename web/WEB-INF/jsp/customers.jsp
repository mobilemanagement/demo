<%-- 
    Document   : customers
    Created on : May 12, 2015, 8:46:56 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <th>CustomerId</th>
            <th>Customer name</th>
            <th>Phone number</th>
            <th>Address</th>
            <th>Email</th>
             <c:forEach items="${customers_list}" var="customers">  
        <tr>  
            <td><c:out value="${customers.customerId}"/></td>  
            <td><c:out value="${customers.customerName}"/></td>  
            <td><c:out value="${customers.phoneNumber}"/></td>      
             <td><c:out value="${customers.address}"/></td>  
              <td><c:out value="${customers.email}"/></td>      
            <td align="center"><a href="<%=request.getContextPath()%>/customers/edit?id=${customer.customerId}">Edit</a> | <a href="<%=request.getContextPath()%>/customers/delete?id=${customer.customerId}">Delete</a></td>  
        </tr>  
    </c:forEach> 
</tr>
        </table>
    </body>
</html>
