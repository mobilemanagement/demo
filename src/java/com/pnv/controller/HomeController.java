/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Administrator
 */
@Controller
public class HomeController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewWelcomePage(ModelMap map) {
        return "home";
    }
}